import React, { useCallback, useEffect, useState } from 'react';
import { FlatList, ListRenderItemInfo, Platform, View } from 'react-native';

import { LoadingIndicator, SafeAreaView } from '../../components';
import { Employee } from '../../components/Employee';
import { useListPersons } from '../../hooks/persons';
import { IEmployee } from '../../types/employee';

import styles from './styles';

export function EmployeesScreen() {
  const [list, setList] = useState<IEmployee[]>([]);
  const { data, error, isLoading, refetch, fetchNextPage, isFetchingNextPage } =
    useListPersons();

  useEffect(() => {
    if (data) {
      setList(data.pages.flatMap(d => d.data));
    }
  }, [data]);

  useEffect(() => {
    if (error) {
      console.error(error);
    }
  }, [error]);

  const handleFetchNextPage = () => {
    if (!isFetchingNextPage && !isLoading) {
      fetchNextPage();
    }
  };

  const renderItem = useCallback(
    ({ item }: ListRenderItemInfo<IEmployee>) => (
      <Employee
        item={{
          email: item.email,
          firstname: item.firstname,
          lastname: item.lastname,
          phone: item.phone,
          website: item.website,
        }}
      />
    ),
    [],
  );

  return (
    <SafeAreaView wide>
      <FlatList<IEmployee>
        refreshing={isLoading}
        onRefresh={refetch}
        data={list}
        numColumns={Platform.OS === 'android' ? 2 : 1}
        keyExtractor={(item, index) => `${item.lastname}-${index}`}
        contentContainerStyle={styles.content}
        renderItem={renderItem}
        onEndReachedThreshold={0.2}
        onEndReached={handleFetchNextPage}
        ListFooterComponent={isFetchingNextPage ? <LoadingIndicator /> : <></>}
        ItemSeparatorComponent={() => <View style={styles.separator} />}
      />
    </SafeAreaView>
  );
}

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  employeeItem: {
    alignItems: 'center',
    flex: 1,
    padding: 8,
    backgroundColor: '#fff',
    borderRadius: 5,
    elevation: 3,
    margin: 8,
  },
  employeeAvatar: {
    alignSelf: 'center',
    height: 48,
    width: 48,
    borderWidth: 1,
    borderRadius: 100,
    marginBottom: 8,
  },
  employeeName: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 8,
  },
  employeeEmail: {
    fontSize: 12,
  },
  IOSemployeeItem: {
    flexDirection: 'row',
    padding: 10,
    paddingVertical: 15,
  },
  employeeInfo: {
    justifyContent: 'space-around',
  },
});

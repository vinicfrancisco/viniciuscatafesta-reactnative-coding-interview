import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import React from 'react';
import { Platform, Text, TouchableOpacity, View } from 'react-native';
import { ScreenParamsList } from '../../navigators/paramsList';
import { IEmployee } from '../../types/employee';
import styles from './styles';

type EmployeesStackProp = StackNavigationProp<ScreenParamsList, 'Employees'>;

interface Props {
  item: IEmployee;
}

const isAndroid = Platform.OS === 'android';
const isIOS = Platform.OS === 'ios';

export const Employee = (props: Props) => {
  const { item } = props;
  const { navigate } = useNavigation<EmployeesStackProp>();

  const goToEmployeeDetail = () => {
    navigate('EmployeeDetail', { employee: item });
  };

  return (
    <TouchableOpacity
      style={isIOS ? styles.IOSemployeeItem : styles.employeeItem}
      onPress={goToEmployeeDetail}>
      <View style={isIOS && styles.employeeInfo}>
        {isAndroid && <View style={styles.employeeAvatar} />}

        <Text style={isAndroid && styles.employeeName}>
          {item.firstname} {item.lastname}
        </Text>

        <Text style={isAndroid && styles.employeeEmail}>{item.email}</Text>
      </View>
    </TouchableOpacity>
  );
};

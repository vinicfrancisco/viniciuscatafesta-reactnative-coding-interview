import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';
import { TouchableOpacity, Text } from 'react-native';
import { DrawerActions, useNavigation } from '@react-navigation/native';
import { EmployeeDetailScreen, EmployeesScreen } from '../screens';
import { ScreenParamsList } from './paramsList';

const Stack = createStackNavigator<ScreenParamsList>();

export function EmployeeNavigator() {
  const { dispatch } = useNavigation();

  return (
    <Stack.Navigator
      screenOptions={{
        headerTintColor: '#6C3ECD',
        headerTitleAlign: 'center',
      }}>
      <Stack.Screen
        component={EmployeesScreen}
        name="Employees"
        options={{
          headerLeft: () => (
            <TouchableOpacity
              onPress={() => dispatch(DrawerActions.openDrawer())}>
              <Text>Drawer</Text>
            </TouchableOpacity>
          ),
        }}
      />
      <Stack.Screen component={EmployeeDetailScreen} name="EmployeeDetail" />
    </Stack.Navigator>
  );
}

import React from 'react';

import { createDrawerNavigator } from '@react-navigation/drawer';
import { TouchableOpacity, Text } from 'react-native';
import { UserProfile } from '../screens';
import { EmployeeNavigator } from './employeesNavigator';

const Drawer = createDrawerNavigator();

export function MainNavigator() {
  return (
    <Drawer.Navigator initialRouteName="EmployeesStack">
      <Drawer.Screen
        component={EmployeeNavigator}
        name="EmployeesStack"
        options={{ title: 'Employees', headerShown: false }}
      />

      <Drawer.Screen
        component={UserProfile}
        name="UserProfile"
        options={{ headerShown: true }}
      />
    </Drawer.Navigator>
  );
}
